﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    public InputField dieInputField, scoreTotalField;
    public Dropdown dropdownField;
    private DieLogicScript dieScript;

    private int[] diceRolls;
    private const int NUMBER_OF_DICE = 5;



	// Use this for initialization
	void Start () {
        dieInputField.onValidateInput += delegate(string input, int charIndex, char addedChar)
        {
            return ValidateDiceFieldEntry(addedChar);
        };

        dieScript = new DieLogicScript();
        diceRolls = new int[NUMBER_OF_DICE];
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    private char ValidateDiceFieldEntry(char charToValidate)
    {
        if (!(charToValidate >= '0' && charToValidate <'9'))
        {
            charToValidate = '\0';
        }
        return charToValidate;
    }

    public void RollRandomDie()
    {
        ClearDieInputField();
        string diceRolls = "";
        int numberOfDie = 0;

        while (numberOfDie < NUMBER_OF_DICE)
        {
            int randomDie = Random.Range(0, 8);
            diceRolls += randomDie.ToString();
            numberOfDie++;
        }

        dieInputField.text = diceRolls;
    }

    public void ScoreDie()
    {
        int category = 0;
        if (DieEntryIsValid())
        {
            category = dropdownField.value;
            diceRolls = GetDieArray(dieInputField.text);

            scoreTotalField.text = dieScript.GetScoreForCategory((DieLogicScript.Category)category, diceRolls).ToString();
        }
        else
        {
            ShowErrorSplash();
        }
    }

    public void GetSuggestion()
    {
        if (DieEntryIsValid())
        {
            diceRolls = GetDieArray(dieInputField.text);
            int enumIndexValue = dieScript.GetSuggestion(diceRolls);
            dropdownField.value = enumIndexValue;
            scoreTotalField.text = dropdownField.captionText.text;
        }
        else
        {
            ShowErrorSplash();
        }
    }

    public void ClearDieInputField()
    {
        dieInputField.text = "";
    }

    //Converts the entered dice into an int array
    private int[] GetDieArray(string inputFieldText)
    {
        for (int i = 0; i < inputFieldText.Length; i++)
        {
            string dieNumber = inputFieldText[i].ToString();
            diceRolls[i] = int.Parse(dieNumber);
        }

        return diceRolls;
    }

    //Assure proper die entry before starting
    private bool DieEntryIsValid()
    {
        bool isValid = false;
        if (dieInputField.text.Length == NUMBER_OF_DICE)
        {
            isValid = true;
        }
        else
        {
            isValid = false;
        }

        return isValid;
    }


    private void ShowErrorSplash()
    {
        EditorUtility.DisplayDialog("Not enough Dice!", "Please check and make sure that you have 5 dice values entered", "ok");
    }

}
