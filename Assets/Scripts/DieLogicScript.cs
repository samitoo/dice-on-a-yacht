﻿using UnityEngine;
using System;
using System.Text.RegularExpressions;
using System.Linq;

public class DieLogicScript {

    private const int NUMERIC_CATEGORIES = 8;
    private const int FULLHOUSE_SCORE = 25;
    private const int SMALLSTRAIGHT_SCORE = 30;
    private const int LARGESTRAIGHT_SCORE = 40;
    private const int ALLDIFFERENT_SCORE = 40;
    private const int ALLSAME_SCORE = 50;

    public enum Category
    {
        ONES,
        TWOS,
        THREES,
        FOURS,
        FIVES,
        SIXES,
        SEVENS,
        EIGHTS,
        THREEOFAKIND,
        FOUROFAKIND,
        FULLHOUSE,
        SMALLSTRAIGHT,
        LARGESTRAIGHT,
        ALLDIFFERENT,
        CHANCE,
        ALLSAME
    }

     //Return a score given a category and dice rolls
        public int GetScoreForCategory(Category category, int[] diceRolls)
        {
            int score = 0;
            
            //If numeric, loop through and add
            if ((int)category < NUMERIC_CATEGORIES)
            {
                for(int i =0; i < diceRolls.Length; i++)
                {
                    //Category Enum is 0 based, so increment to calculate
                    if(diceRolls[i] == (int)category + 1)
                    {
                        score += diceRolls[i];
                    }
                }
            }
            //For non numeric categories
            else
            {
                int[] arrayOfDifferences;
                Array.Sort(diceRolls);
                
                //Create an array of differences comparing values
                arrayOfDifferences = GetArrayOfDifferences(diceRolls);
                
                switch (category)
                {
                    case (Category.THREEOFAKIND):
                        score = ScoreOfAKind(arrayOfDifferences, diceRolls, 2);
                        break;
                    case (Category.FOUROFAKIND):
                        score = ScoreOfAKind(arrayOfDifferences, diceRolls, 3);
                        break;
                    case (Category.FULLHOUSE):
                        if (IsFullHouse(arrayOfDifferences))
                        {
                            score = FULLHOUSE_SCORE;
                        }
                        else
                        {
                            score = 0;
                        }
                        break;
                    case (Category.SMALLSTRAIGHT):
                        score = ScoreSmallOrLargeStraight(arrayOfDifferences, Category.SMALLSTRAIGHT);
                        break;
                    case (Category.LARGESTRAIGHT):
                        score = ScoreSmallOrLargeStraight(arrayOfDifferences, Category.LARGESTRAIGHT);
                        break;
                    case (Category.ALLDIFFERENT):
                        score = ScoreAllDifferent(arrayOfDifferences);
                        break;
                    case (Category.CHANCE):
                        score = ScoreChance(diceRolls);
                        break;
                    case (Category.ALLSAME):
                        score = ScoreAllSame(arrayOfDifferences);
                        break;
                    default:
                        Debug.Log("Unrecognized Category value.");
                        break;
                }
            }
            return score;
        }

        //Test all categories and see which returns the highest possible score
        public int GetSuggestion(int[] diceRolls)
        {
            int highestScore = 0;
            Category highestCategory = new Category();

            foreach(var category in Enum.GetValues(typeof(Category)).Cast<Category>())
            {
                int categoryScore = GetScoreForCategory(category, diceRolls);

                if (categoryScore > highestScore)
                {
                    highestScore = categoryScore;
                    highestCategory = category;
                }
            }

            return (int)highestCategory;
            
        }


        //Checks for Three or Four of a Kind
        private int ScoreOfAKind(int[] diceRollDifferences, int[] diceRolls, int numberOfNeededZeros)
        {
            int numberOfZeros = 0;
            int zerosInARow = 0;
            int score = 0;

            //Check for zeros in a row from difference array to indicate same die in an ordered set
            for(int i = 0; i < diceRollDifferences.Length; i++)
            {
                if (diceRollDifferences[i] == 0)
                {
                    numberOfZeros++;
                    if (numberOfZeros >= 2)
                    {
                        zerosInARow = numberOfZeros;
                    }
                }
                else
                {
                    numberOfZeros = 0;
                }
            }

            if (zerosInARow >= numberOfNeededZeros)
            {
                score = AddAllDiceRolls(diceRolls);
            }
            else
            {
                score = 0;
            }

            return score;
        }

        
        //Compares the differences pattern of the sorted array looking for sequential values
        private int ScoreSmallOrLargeStraight(int[] diceRollDifferences, Category categoryType)
        {
            int score = 0;
            int sequenceCount = 0;

            for (int i=0; i< diceRollDifferences.Length; i++)
            {
                if (diceRollDifferences[i] == 1)
                {
                    sequenceCount++;
                }
                //Less than a small straight found in a row
                else if(sequenceCount < 3)
                {
                    sequenceCount = 0;
                }
            }

            if(sequenceCount >= 3 && categoryType == Category.SMALLSTRAIGHT)
            {
                score = SMALLSTRAIGHT_SCORE;
            }
            else if (sequenceCount == 4 && categoryType == Category.LARGESTRAIGHT)
            {
                score = LARGESTRAIGHT_SCORE;
            }
            else
            {
                score = 0;
            }

            return score;
          
        }

        private int ScoreAllDifferent(int[] diceRollDifferences)
        {
            int score = 0;
            bool areAllDifferent = true;
            //If the difference between i+1 and i is 0, they are the same, so not all different. 
            for (int i = 0; i < diceRollDifferences.Length; i++)
            {
                if (diceRollDifferences[i] == 0)
                {
                    score = 0;
                    areAllDifferent = false;
                }
                else if (areAllDifferent)
                {
                    score = ALLDIFFERENT_SCORE;
                }
            }

            return score;
        }

        private int ScoreChance(int[] diceRolls)
        {
            return AddAllDiceRolls(diceRolls);
        }

        private int ScoreAllSame(int[] diceRollDifferences)
        {
            int score = 0;
            bool areAllSame = true;
            //If the difference between i+1 and i is 0, they are the same number.  If we get all 0's, we have all the same dice rolls.
            for (int i = 0; i < diceRollDifferences.Length; i++)
            {
                if (diceRollDifferences[i] != 0)
                {
                    score = 0;
                    areAllSame = false;
                }
                else if (areAllSame)
                {
                    score = ALLSAME_SCORE;
                }

            }
            return score;
        }

        //Create an array that compares values of the rolls.  Used to check for sequential values.
        private int[] GetArrayOfDifferences(int[] diceRolls)
        {
            int rollDifference = 0;
            int[] arrayOfDifferences = new int[4];

            for (int i = 1; i < diceRolls.Length; i++)
            {
                rollDifference = diceRolls[i] - diceRolls[i - 1];
                arrayOfDifferences[i - 1] = rollDifference;
            }

            return arrayOfDifferences;
        }

        //Check for a Fullhouse pattern 0010 or 0100 with regex
        private bool IsFullHouse(int[] diceRollDifferences)
        {
            bool matchfound = false;
            string fullHousePattern = @"00\d0|0\d00";
            string differencePattern = "";

            for (int i = 0; i < diceRollDifferences.Length; i++)
            {
                differencePattern += diceRollDifferences[i];
            }

            Regex regex = new Regex(fullHousePattern);
            Match match = regex.Match(differencePattern);
            if (match.Success)
            {
                matchfound = true;
            }
            else
            {
                matchfound = false;
            }

            return matchfound;
        }

        //Sum all of the dice rolls
        private int AddAllDiceRolls(int[] diceRolls)
        {
            int score = 0;
            for (int i = 0; i < diceRolls.Length; i++)
            {
                score += diceRolls[i];
            }

            return score;
        }

    }
